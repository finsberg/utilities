import yaml


fname = "yaml_test.yml"

d = {"test":range(10)}

with open(fname, "wb") as f:
    yaml.dump(d, f, default_flow_style=False)

with open(fname, "r") as f:
    d_load = yaml.load(f)

print(d_load)
