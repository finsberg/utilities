"""
Suppose you have a repository with the following strucutre:
repo/repo

mkdir docs
cd docs 
sphinx-quickstart # follow the instructions
sphinx-apidoc -o . ../repo

make html

"""


def test():
    """

    Inline math :math:`a \in \Xi_{\mathrm{sim}}` this works

    Noninline math

    .. math:: 
    
       \Xi_{\mathrm{seg}} 

    Rember double space

    Notes

    .. note::

       Valvular events has to be parsed in the initialization of this class.

    like this

    Tables

    +------------+------------+-----------+
    | Header 1   | Header 2   | Header 3  |
    +============+============+===========+
    | body row 1 | column 2   | column 3  |
    +------------+------------+-----------+
    | body row 2 | Cells may span columns.|
    +------------+------------+-----------+
    | body row 3 | Cells may  | - Cells   |
    +------------+ span rows. | - contain |
    | body row 4 |            | - blocks. |
    +------------+------------+-----------+

    And code:

    .. code-block:: python
    :emphasize-lines: 3,5

    def some_function():
        interesting = False
        print 'This line is highlighted.'
        print 'This one is not...'
        print '...but this one is.'

    is written like this

    *Example of usage*::

      # Create generator
      solutions = solver.solve((0.0, 1.0), 0.1)

      # Iterate over generator (computes solutions as you go)
      for (interval, vs) in solutions:
        # do something with the solutions


    :param mesh: The mesh
    :type mesh: :py:class:`dolfin.Mesh`
    :param patient_parameters: the parameters 
    :type patient_parameters: dict or :py:function:`dolfin.parameters`
    :param ffun: Facet function
    :type ffun: :py:class:`dolfin.MeshFunction`
    :param int endo_lv_marker: The marker of en endocardium
    :param u: Displacement
    :type u: :py:class:`dolfin.Function`
    :returns vol: Volume of inner cavity
    :rtype: float
    """
    pass



def generate_sphix_table(params):

    from tabulate import tabulate
    table = [[k,v, ""] for k,v in params.to_dict().iteritems()]
    # from IPython import embed; embed()
    print tabulate(table, headers=['key', 'Default Value', "Description"], tablefmt='grid')

def test_table():

    from dolfin import Parameters
    params = Parameters("test_table")
    
    params.add("active_model", "active_stress", ["active_strain",
                                                 "active_strain_rossi",
                                                 "active_stress"])
    params.add("state_space", "P_2:P_1")
    params.add("nonzero_initial_guess", True)
    params.add("base_spring_k", 1.0)
    params.add("iteration", 0)

    material_parameters = Parameters("Material_parameters")
    material_parameters.add("a", 2.28)
    material_parameters.add("a_f", 1.685)
    material_parameters.add("b", 9.726)
    material_parameters.add("b_f", 15.779)
    params.add(material_parameters)

    from IPython import embed; embed()
    exit()
    generate_sphix_table(params)

def main():
    from pulse_adjoint.setup_optimization import setup_application_parameters, setup_patient_parameters, setup_optimization_parameters

    params = setup_optimization_parameters() #setup_patient_parameters()# setup_application_parameters()
    generate_sphix_table(params)

if __name__ == "__main__":
    # test_table()
    main()
