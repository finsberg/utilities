#!/bin/bash

# These variables are dependent on the installation you require
dep_version="2017.2.0"
dolfin_version="2017.2.0"

# System-dependent variables
env_name=dolfin_venv # Name of virtual-enviroment
# env_activate=dolfin_activate # Activation-command
git_src=https://bitbucket.org # Git user
num_threads=12 # Number of threads used in parallel

# We assume that SWIG and BOOST are already installed,
# if not then you need to install that as well.


# Creating virtual enviroment
mkdir $env_name;
cd $env_name
virtualenv -p python3 env
source env/bin/activate

#Reset enviroment variables
unset PETSC_DIR
unset Eigen3_DIR
unset SLEPC_DIR
unset PETSC_ARCH
unset PYBIND11_DIR

# Set lib path
export LD_LIBRARY_PATH=$VIRTUAL_ENV/lib:$LD_LIBRARY_PATH

wget -nc --quiet https://github.com/pybind/pybind11/archive/v2.2.3.tar.gz
tar -xf v2.2.3.tar.gz
cd pybind11-2.2.3
mkdir build; cd build
cmake -DPYBIND11_TEST=off -DCMAKE_INSTALL_PREFIX=$VIRTUAL_ENV ../
make install
cd ../..

# Downloading and installing PETSC
VERSION="3.8.4"
wget -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${VERSION}.tar.gz
tar xfz petsc-lite-${VERSION}.tar.gz
cd petsc-${VERSION}
python2 ./configure "--prefix=$VIRTUAL_ENV" \
	--COPTFLAGS="-O2" \
        --CXXOPTFLAGS="-O2" \
        --FOPTFLAGS="-O2" \
        --with-debugging=0 \
        --with-shared-libraries \
        --download-blacs \
        --download-hypre \
        --download-metis \
        --download-mumps \
        --download-ptscotch \
        --download-scalapack \
        --download-spai \
        --download-suitesparse \
        --download-superlu \
	--download-parmetis \
	--download-fblaslapack \	
        --download-mpich \
	--download-hdf5 \
	--download-ml

make MAKE_NP=$num_threads all
make MAKE_NP=$num_threads install
export PETSC_DIR=$VIRTUAL_ENV

cd ..

# Install Eigen
EIGEN_VERSION="3.3.3"
rm -rf eigen.tar.bz2 eigen/
wget --read-timeout=10 -nc http://bitbucket.org/eigen/eigen/get/${EIGEN_VERSION}.tar.bz2 -O eigen.tar.bz2
mkdir -p eigen
tar -xf eigen.tar.bz2 -C eigen --strip-components=1
cd eigen
mkdir -p build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=${VIRTUAL_ENV}
make install
cd ../..

# Downloading and installing SLEPC
VERSION=3.8.3
wget -nc http://slepc.upv.es/download/distrib/slepc-${VERSION}.tar.gz
tar xfz slepc-${VERSION}.tar.gz
cd slepc-${VERSION}
python2 ./configure "--prefix=$VIRTUAL_ENV"
make MAKE_NP=$num_threads SLEPC_DIR=$PWD
make MAKE_NP=$num_threads SLEPC_DIR=$PWD PETSC_DIR=$VIRTUAL_ENV install
make MAKE_NP=$num_threads SLEPC_DIR=$VIRTUAL_ENV PETSC_DIR=$VIRTUAL_ENV test
export SLEPC_DIR=$VIRTUAL_ENV
cd ..

# Creating requirement file for virtualenv
cat > requirements.txt << EOL
git+$git_src/fenics-project/fiat.git@$dep_version
git+$git_src/fenics-project/ufl.git@$dep_version
git+$git_src/fenics-project/dijitso.git@$dep_version
git+$git_src/fenics-project/instant.git@$dep_version
git+$git_src/fenics-project/ffc.git@$dep_version
petsc4py==3.8.1
slepc4py==3.8.0
matplotlib
pytest
pytest-xdist
scipy
sphinx
ply
decorator
numpy
sympy<1.2
jupyter
ipynb
mpi4py
seaborn
flake8
pygmsh
meshio
EOL

# Installing requirements
pip install -r requirements.txt --prefix=$VIRTUAL_ENV --upgrade --no-cache-dir

# Install dolfin
git clone $git_src/fenics-project/dolfin.git
cd dolfin; git checkout $dolfin_version 
mkdir -p build; cd build;
cmake -DCMAKE_INSTALL_PREFIX=$VIRTUAL_ENV -DCMAKE_BUILD_TYPE=Developer ..
make -j $num_threads install

# Dolfin python layer
cd ../python
export PYBIND11_DIR=$VIRTUAL_ENV
pip3 -v install -e .
cd ../..

# Install mshr

# It needs gmp
wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.bz2
tar -vf gmp-6.1.2.tar.bz2
cd gmp-6.1.2/
./configure --prefix=$VIRTUAL_ENV
make
make check
make install
cd ..

# And mpfr
wget https://www.mpfr.org/mpfr-current/mpfr-4.0.1.tar.bz2
tar -xf mpfr-4.0.1.tar.bz2
cd mpfr-4.0.1
./configure --prefix=$VIRTUAL_ENV --with-gmp=$VIRTUAL_ENV
make
make check
make install

# Finally install mshr
git clone $git_src/fenics-project/mshr && cd mshr && git checkout $dolfin_version
mkdir -p build; cd build;
cmake -DCMAKE_INSTALL_PREFIX=$VIRTUAL_ENV ..
make -j $num_threads install

# mshr python layer
cd ../python
pip3 -v install -e .
cd ../..


cat > $env_name.conf << EOL
#!/bin/bash
source $VIRTUAL_ENV/bin/activate
source $VIRTUAL_ENV/share/dolfin/dolfin.conf
export PYBIND11_DIR=$VIRTUAL_ENV
export PETSC_DIR=$VIRTUAL_ENV
export SLEPC_DIR=$VIRTUAL_ENV
export OMPI_MCA_btl_base_warn_component_unused=0
EOL
