from dolfin import *
import os

try:
    import h5py
    has_h5py = True
except:
    has_h5py = False
    print("h5py not found. Install this if you want to overwrite existing groups")

def save(h5name, h5group):
    """
    Save example data to a file

    :param str h5name: path to the file
    :param str h5group: The main group within the file

    """

    # Make some dummy data
    mesh = UnitSquareMesh(3,3)
    
    V = FunctionSpace(mesh, "CG", 1)
    W = FunctionSpace(mesh, "CG", 2)

    exp1 = Expression("sin(x[0])", element = V.ufl_element())
    exp2 = Expression("cos(x[0])", element = W.ufl_element())
    u1 = interpolate(exp1, V)
    u2 = interpolate(exp2, W)

    # Put each function in its own subgroup
    group1 = "{}/u_1".format(h5group)
    group2 = "{}/u_2".format(h5group)

    # Append to the file if it allready exist, otherwise create a new file
    file_mode = "a" if os.path.isfile(h5name) else "w"

    # If you want to overwrite an existing group,
    # you need to first delete the old one
    # HDF5File cannot do this, but we can use h5py
    if file_mode == "a":
        if has_h5py:
            with h5py.File(h5name) as h5file:
                if h5group in h5file:
                    print("Deleting existing group: '{}'".format(h5group))
                    del h5file[h5group]


    # Open file and write
    with HDF5File(mpi_comm_world(), h5name, file_mode) as h5file:

        h5file.write(u1, group1)
        h5file.write(u2, group2)
    

def load(h5name, h5group):
    """
    Load example data from a file

    :param str h5name: path to the file
    :param str h5group: The main group within the file

    """

    mesh = UnitSquareMesh(3,3)

    # You need to know the functionspaces in advance
    V = FunctionSpace(mesh, "CG", 1)
    W = FunctionSpace(mesh, "CG", 2)
    u1 = Function(V)
    u2 = Function(W)

    group1 = "{}/u_1".format(h5group)
    group2 = "{}/u_2".format(h5group)

    # Open file and read
    with HDF5File(mpi_comm_world(), h5name, "r") as h5file:

        h5file.read(u1, group1)
        h5file.read(u2, group2)

    plot(u1)
    plot(u2)
    interactive()
    
    

def main():

    h5name = "test.h5"
                  
    h5group = "time_0"
    save(h5name, h5group)

    h5group = "time_1"
    save(h5name, h5group)

                  
    load(h5name, h5group)


    
    

if __name__ == "__main__":
    main()
