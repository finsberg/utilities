\documentclass[tikz,border=0cm]{standalone}
\usepackage{type1cm}
\usepackage{fp}
\usepackage{bm}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{calc}
\usetikzlibrary{shadows}

\usepackage{fetamont}
%\usepackage[scaled]{helvet}
%\usepackage{avant}
%\renewcommand{\familydefault}{\sfdefault}
\usepackage{cmbright}

\usepackage[letterspace=150]{microtype}

\edef\myfontscale{1.7}
\definecolor{simula}{RGB}{245,130,32}
\definecolor{amaranth}{rgb}{0.9, 0.17, 0.31}
\definecolor{amber}{rgb}{1.0, 0.75, 0.0}

\input{rescalefonts}
\input{defs}

\usepackage{booktabs,dcolumn}
\usepackage{array}
\newcolumntype{T}{>{\begingroup\bfseries}r<{\endgroup}}
\newcolumntype{N}{D{.}{.}{-1}}
\newcolumntype{R}{D{.}{.}{2.4}}
\newcolumntype{K}{D{.}{.}{3.4}}
\newcommand{\imgcase}[2]{\includegraphics[width=0.5\linewidth]{#1_#2_base}%
\includegraphics[width=0.5\linewidth]{#1_#2_side}}
\usepackage[tableposition=top,font=footnotesize,labelfont=bf,sf]{caption}

\usepackage[margin=1in, paperwidth=48in, paperheight=48in]{geometry}

\begin{document}

\begin{tikzpicture}[x=120cm, y=120cm]
\fill[white, use as bounding box] (0, 0) rectangle (1, 1);

%%%% title %%%%
\begingroup
\pgfmathsetseed{3674}
\node[decoration={random steps, segment length=1cm, amplitude=3mm}, decorate,
      fill=simula, inner sep=1cm,
      drop shadow={shadow yshift=1ex, shadow xshift=-1ex},
      anchor=north, draw=none, line width=0mm]
      (title)
      at ($(current bounding box.north)-(0,2cm)$)
  {\begin{minipage}{0.88\textwidth}
   \begin{center}
   {\ffmfamily\bfseries\color{black}
   {\LARGE Patient--specific parameter estimation} \\[0.4ex]
   {\LARGE for a transversely isotropic active strain model} \\[0.4ex]
   {\LARGE of left ventricular mechanics}}
   
   \bigskip
   {\color{white}
   \textbf{S.~Gjerald, J.~Hake, \underline{S.~Pezzuto}, J.~Sundnes, S.~T.~Wall}}
   
   \medskip
   Simula Research Laboratory, 1235 Lysaker, Norway
   
   \medskip
   \texttt{\{sjurug,hake,simonep,sundnes,samwall\}@simula.no}
   \end{center}
  \end{minipage}};
\endgroup

%%%% introduction %%%%
\node[anchor=north west]
(motif title)
at ($(current bounding box.north west)+(2cm,-18cm)$)
{\bfseries\ffmfamily\Large\underline{Introduction}};

\node[draw=none, anchor=north west]
(motif text)
at ( $(motif title.south west)+(1cm,0)$ )
{\begin{minipage}{0.9\textwidth}
\footnotesize
With the advent of improved methods and increased computing power, patient specific mathematical modeling of the heart has promise to aid in medical diagnosis and in planning of patient treatment.   However, due to the complexity of a single heartbeat, significant patient data is needed to accurately parameterize such models to give realistic predictions of cardiac behavior.  Models built from sparser data sets would be highly useful if they could accurately predict specific aspects of the heart, such as the mechanical motion.  In this work, we attempt to use geometric data at the end-systolic and end-diastolic state, along with cardiac fiber architecture, to parameterize a mechanical model of the heart to best match surface deformation over the cardiac cycle.  If accurate, such a process would allow determining mechanical properties at the level of the fiber architecture and could give insight into function that cannot be measured using current imaging tools.  

%Given our cardiac mechanics model 
%$f\bigl(p_\mathsf{inner}, V_\mathsf{inner}; \alpha\bigr) = 0$ 
%to simulate the $PV$--loop, we seek for the set of parameters
%$\alpha$ such that endo-- and epi--cardial surfaces best match
%the target geometry.
\end{minipage}};

%%%% methods %%%%
\node[anchor=north west]
(methods title)
at ($(motif text.south west)-(1cm,1ex)$)
{\bfseries\ffmfamily\Large\underline{Methods}};

%%%% geometry %%%%
\node[draw=simula, line width=1mm, anchor=north west,
      rounded corners=4mm, inner sep=1cm,
      fill=gray!10!white, minimum height=20cm]
(geometry)
at ( $(methods title.south west)+(1cm,0)$ )
{\begin{minipage}{56cm}
\vskip2ex
\begin{center}
\includegraphics[height=10cm]{flowchart}
\end{center}

\footnotesize
The epicardial and endocardial surfaces were given as point clouds
that were triangularized (A). One node was added at the apex to cover the
apical hole. A plane was fitted to the base of the endocardium by a least
squares method (B). The basal nodes of both endocardium and epicardium were
projected into this plane, and the heart was moved and rotated so that the
plane coincided with the $yz$--plane at $x=0$. Then the cardial wall was
tetrahedralized with {\ffmfamily Gmsh} (C). Fiber orientations were provided
and interpolated at the nodes and integration points of the final mesh (D).
\end{minipage}};

\node[anchor=east, rounded corners=3mm, fill=simula]
(geometry title)
at ($ (geometry.north east)!3cm!(geometry.north west) $)
{\ffmfamily Geometry pre--processing};

%%%% mechanics %%%%
\node[draw=simula, line width=1mm, anchor=north west,
      rounded corners=4mm, inner sep=1cm,
      fill=gray!10!white, minimum height=50cm]
(mech)
at ($(geometry.north east)+(2cm,0)$)
{\begin{minipage}{52cm}
\footnotesize
Myocardium modeled as hyperelastic, incompressible and transversely isotropic
material.

\[
\cW(\tC) = \frac{a}{2b}\left(e^{b(\cI_1-3)}-1\right)
+ \frac{\aaf}{2\bbf}\left(e^{\bbf(\cI_{4,\vfo}-1)_+^2}-1\right)
\]

\includegraphics[width=0.4\textwidth]{actstrain}

\begin{equation} \label{eq:vcontrol}
\mathcal{L}(\vphi, p, \pinner; \gamma, \Vinner) :=
\int_{\Omega_0} 
\cW\bigl(\mtsp{\tFa}\tC\tFa^{-1}\bigr) \det(\tFa)\:\ddV
- \int_{\Omega_0} p(J -1) \: \ddV
- \pinner \bigl(\mathcal{V}(\vphi) - \Vinner \bigr),
\end{equation}

\begin{equation} \label{eq:pcontrol}
\mathcal{L}(\vphi, p; \gamma, \pinner) :=
\int_{\Omega_0} 
\cW\bigl(\mtsp{\tFa}\tC\tFa^{-1}\bigr) \det(\tFa)\:\ddV
- \int_{\Omega_0} p(J -1) \: \ddV
- \pinner \mathcal{V}(\vphi).
\end{equation}

\end{minipage}};

\node[anchor=west, rounded corners=3mm, fill=simula]
(mech title)
at ($ (mech.north west)!3cm!(mech.north east) $)
{\ffmfamily Mechanical model};

%%%% parameter %%%%
\node[draw=simula, line width=1mm, anchor=north west,
      rounded corners=4mm, inner sep=1cm,
      fill=gray!10!white, minimum height=27.4cm]
(parameter)
at ($(geometry.south west)+(0,-2cm)$)
{\begin{minipage}{56cm}
\footnotesize

\begin{minipage}[t]{0.5\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{paramfit}
\end{minipage}\hfill
\begin{minipage}[t]{0.48\textwidth}
\vspace{5mm}
With limited data available, we are left to make
reasonable \emph{a priori} choices based on values from the literature for some
parameters, and fit the remaining to match the given pressures and volumes.

\bigskip
\textbf{$\triangleleft$ Figure:}
Fitting (testcase 1017) was performed as follows:

\begin{enumerate}
\renewcommand{\theenumi}{\Alph{enumi}}
\item The ventricle is passively inflated, by means of 
      model~{\color{amber}\bfseries(\ref{eq:pcontrol})}
      with $\gamma=0$, to the given target pressure;

\item $\aaf$ is iteratively adjusted until the volume matched the target volume,
      using~{\color{amber}\bfseries(\ref{eq:pcontrol})}.
      An interpolation method around the target volume 
      is used to select the next value for $\aaf$, which always converged in 
      three iterations or less;

\item To reach the end--systolic point, we hold the volume constant by means 
      of~{\color{amaranth}\bfseries(\ref{eq:vcontrol})}, at the \textbf{ED} value,
      while we increase~$\gamma$ until 
      the target \textbf{ES} pressure is reached;

    \item We eventually keep the pressure constant,
      using~{\color{amber}\bfseries(\ref{eq:pcontrol})},
      while increasing $\gamma$ further until the volume also matched 
      the target \textbf{ES} value.
\end{enumerate}

\end{minipage}

\end{minipage}};

\node[anchor=east, rounded corners=3mm, fill=simula]
(parameter title)
at ($ (parameter.north east)!3cm!(parameter.north west) $)
{\ffmfamily Parameter estimation};

%%%% results %%%%
\node[anchor=north west]
(results title)
at ($(parameter.south west)-(1cm,1ex)$)
{\bfseries\ffmfamily\Large\underline{Results}};

\node[draw=none, anchor=north west]
(results tab1)
at ( $(results title.south west)+(1cm,0)$ )
{\begin{minipage}{0.3\textwidth}
\begin{table}
\caption*{Comparison of global geometrical quantities between the reference (or target)
geometry and the simulation for all the cases at the end--systolic and end--diastolic
stages. The last columns represents the Hausdorff distance between the simulation 
and the target geometry of the endocardium and the epicardium respectively.}
\scriptsize
\centering
\begin{tabular}{@{}TcRRRRKKRR@{}}
\toprule
& &
\multicolumn{2}{c}{\centering Thickness [cm]} &
\multicolumn{2}{c}{Length [cm]} &
\multicolumn{2}{c}{Wall volume [m$\ell$]} &
\multicolumn{2}{c}{Distance [cm]} \\
\cmidrule(lr){3-4}
\cmidrule(lr){5-6}
\cmidrule(lr){7-8}
\cmidrule(lr){9-10}

\multicolumn{1}{c}{Case} &
\multicolumn{1}{c}{Stage} &
\multicolumn{1}{c}{\itshape ref} &
\multicolumn{1}{c}{\itshape sim} &
\multicolumn{1}{c}{\itshape ref} &
\multicolumn{1}{c}{\itshape sim} &
\multicolumn{1}{c}{\itshape ref} &
\multicolumn{1}{c}{\itshape sim} &
\multicolumn{1}{c}{\itshape endo} & 
\multicolumn{1}{c}{\itshape epi} \\
\midrule
0912 & ED & 0.808 & 0.821 & 5.445 & 5.480 & 43.336 & 42.633 & 0.275 & 0.249 \\
     & ES & 0.909 & 0.995 & 5.372 & 5.304 & 45.423 & 42.633 & 0.355 & 0.346 \\[1ex]

0917 & ED & 0.718 & 0.748 & 5.008 & 4.993 & 32.742 & 31.127 & 0.266 & 0.251 \\
     & ES & 0.746 & 0.885 & 4.942 & 4.727 & 32.665 & 31.127 & 0.423 & 0.336 \\[1ex]

1017 & ED & 0.782 & 0.777 & 5.474 & 5.550 & 39.927 & 39.933 & 0.205 & 0.274 \\
     & ES & 0.827 & 0.833 & 5.365 & 5.464 & 37.146 & 39.933 & 0.237 & 0.236 \\[1ex]

1024 & ED & 0.984 & 0.966 & 5.275 & 5.316 & 41.885 & 41.611 & 0.097 & 0.089 \\
     & ES & 1.042 & 1.031 & 5.238 & 5.301 & 42.054 & 41.611 & 0.306 & 0.332 \\
\bottomrule
\end{tabular}
\end{table}
\end{minipage}};

\node[draw=none, anchor=north west]
(results fig)
at ( $(results tab1.north east)+(2cm,0)$ )
{\begin{minipage}{0.3\textwidth}
\scriptsize
\begin{tabular}{T@{\quad}m{0.43\textwidth}@{\quad}m{0.43\textwidth}}
\multicolumn{1}{c}{Case} & 
\multicolumn{1}{c}{End--diastole} & 
\multicolumn{1}{c}{End--systole} \\
0912 & \imgcase{0912}{ED} & \imgcase{0912}{ES} \\
0917 & \imgcase{0917}{ED} & \imgcase{0917}{ES} \\
1017 & \imgcase{1017}{ED} & \imgcase{1017}{ES} \\
1024 & \imgcase{1024}{ED} & \imgcase{1024}{ES} \\
\end{tabular}
\end{minipage}};

\node[draw=none, anchor=north]
(results tab2)
at (results tab1.south)
{\begin{minipage}{0.3\textwidth}

\begin{table}
\caption*{Summary of the parameters we used 
to reach the target pressure and target volume for the 
end diastolic and end systole stage.
For convenience we report the given target pressures and target volumes
which were used to fit the parameters.}
\centering
\scriptsize
\begin{tabular}{@{}TNNNNNNNNN@{}}
\toprule
&
\multicolumn{5}{c}{Parameters ($a$ and $\aaf$ in [kPa])} &
\multicolumn{2}{@{\quad}c@{\quad}}{Pressure [kPa]}   &
\multicolumn{2}{@{\quad}c@{\quad}}{Volume [m$\ell$]} \\ 
\cmidrule(lr){2-6}
\cmidrule(lr){7-8}
\cmidrule(lr){9-10}

\multicolumn{1}{l}{Case} &
\multicolumn{1}{c}{$a$} &
\multicolumn{1}{c}{$b$} &
\multicolumn{1}{c}{$\aaf$} &
\multicolumn{1}{c}{$\bbf$} &
\multicolumn{1}{c}{$\gamma$} &
\multicolumn{1}{c}{ED} & 
\multicolumn{1}{c}{ES} &
\multicolumn{1}{c}{ED} & 
\multicolumn{1}{c}{ES} \\
\midrule
0912 & 0.1181 &  5.405 &  4.33 & 0.876 & 0.208 & 0.7  & 10.8 & 28.03 & 15.25 \\
0917 & 0.2362 & 10.81  &  2.85 & 0.876 & 0.213 & 0.62 & 10.3 & 19.11 & 11.93 \\
1017 & 0.2362 & 10.81  &  3.39 & 0.876 & 0.162 & 0.6  &  8.8 & 22.67 & 17.29 \\
1024 & 0.2362 & 10.81  & 14.77 & 0.876 & 0.086 & 0.67 &  8.1 & 18.28 & 14.24 \\
\bottomrule
\end{tabular}
\end{table}
\end{minipage}};

\node[draw=none, anchor=north west]
(results fig text)
at ($(results fig.north east)+(0cm,-1cm)$)
{\begin{minipage}{37cm}
\footnotesize
\textbf{$\triangleleft$ Figure:}
Displacements applied to the DS geometry, read solid
surfaces, from the four cases (0912, 0917, 1017, 1024) and two stages
(ED, ES) are compared with the epi/endo triangulation, transparent
surfaces, of the given point cloud for each stage/case.
\end{minipage}};

%%%% references %%%%
\node[anchor=north west]
(reference title)
at ($(results fig text.south west)-(0cm,2cm)$)
{\bfseries\ffmfamily\Large\underline{References}};

\node[draw=black, anchor=north west]
(reference text)
at ( $(reference title.south west)+(1cm,0)$ )
{\begin{minipage}{37cm}
\footnotesize
Ref.~1

Ref.~2

Ref.~3

Ref.~4
\end{minipage}};

%%%% software %%%%
\node[anchor=north west]
(software title)
at ($(reference text.south west)-(1cm,2cm)$)
{\bfseries\ffmfamily\Large\underline{Software}};

\node[draw=none, anchor=north west]
(software text)
at ( $(software title.south west)+(1cm,0)$ )
{\begin{minipage}{37cm}
\footnotesize
\includegraphics[height=3cm]{logofenics}
\includegraphics[height=3cm]{qrfenics}
\hspace{1cm}
\includegraphics[height=3cm]{logogmsh}
\includegraphics[height=3cm]{qrgmsh}
\hspace{1cm}
\includegraphics[height=3cm]{logoparaview}
\includegraphics[height=3cm]{qrparaview}
\end{minipage}};


%%%% footer %%%%
\begingroup
\pgfmathsetseed{31417}
\fill[simula,
      drop shadow={shadow yshift=1ex, shadow xshift=-1ex}]
decorate[decoration={random steps, segment length=1cm, amplitude=4mm}]
{ ($(current bounding box.south west)+(-1cm,4cm)$) --
  ($(current bounding box.south east)+(1cm,4cm)$) }
-- (current bounding box.south east) --
   (current bounding box.south west) -- cycle;
\node[anchor=west]
at ( $(current bounding box.south west)+(1cm,2cm)$ )
{\includegraphics[height=4cm,trim=0 0 400 0,clip]{simulalogo}};
\node[anchor=east]
at ( $(current bounding box.south east)+(-1cm,2cm)$ )
{\includegraphics[height=4cm,trim=400 0 0 0,clip]{simulalogo}};
\endgroup

\end{tikzpicture}

\end{document}
